def biggest_prime(n: int):
    """find the largest prime less than n"""
    i = 1
    big_prime = 1
    while i < n:

        is_prime = True

        k = 2
        while k < i:
            if i % k == 0:
                is_prime = False
            k += 1

        if is_prime:
            big_prime = i
        i += 1

    return big_prime


print (biggest_prime(100))