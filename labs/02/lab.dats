#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"

//we always need this to make c happy
implement main0() = ()

//lab doc availible at https://docs.google.com/document/d/1A38oeRA0pWLuvMgCb23J6Lz8s88RmOHiCM6vY1182nI/edit



//form last time: How to print?

fun is_even(n:int):bool =
  if (n = 0)
  then (true)
  else (if n = 1
  then false
  else
  let
    val () = (println!(n))
  in
    (is_even(n - 2))
  end)

val () = println!(is_even(10))




fun repeat_twice(f: int -> int): int -<cloref1> int = lam n => f(f(n))

fun square(n: int): int = (n*n)

val h = repeat_twice(square)

val () = println!(h(3))


fun repeat(f: int -> int, n:int): int -<cloref1> int =
  if n = 0
  then lam x => x
  else lam x => repeat(f,n-1)(f(x))


val () = println!("ans")
val () = println!(repeat(square, 3)(2))

datatype
list_int =
 | list_nil  of ()
 | list_cons of (int, list_int )


fun length(ls:list_int):int =
  case+ ls of
  | list_nil()      => 0
  | list_cons(_,xs) => 1 + length(xs)

val ls = list_cons(1,list_cons(2, list_nil()))
val () = println!(length(ls))

datatype
tree =
  | empty of ()
  | node of (tree, int, tree)

fun size(t:tree):int =
  case+ t of
  | empty() => 0
  | node(rtree, int, ltree) => 1 + size(rtree) + size(ltree)

val ls = node(empty(),2,node(empty(),2,empty()))
val () = println!(size(ls))

