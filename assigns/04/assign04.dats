(* ****** ****** *)
//
// Title:
// Concepts of
// Programming Languages
// Course: CAS CS 320
//
// Semester: Summer I, 2018
//
// Classroom: CAS 220
// Class Time: MTWR 2:00-4:00
//
// Instructor: Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Assign03: 25 points
//
// Out date: Tue, Jun 05, 2018
// Due date: Tue, Jun 12, 2018 // by midnight
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS\
/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

#define
ATS_PACKNAME "ASSIGN04"

(* ****** ****** *)
//
// intrep for
// representing
// non-negative integers
//
typedef intrep = list0(int)
//
(* ****** ****** *)
//
#define BASE 10
// HX: this may change!!!
//
(* ****** ****** *)
//
// For instance,
//
// For BASE=10
// [] represents 0
// [1] represents 1
// [2,1] represents 12
// [3,2,1] represents 123
//
// For BASE = 2
// [] represents 0
// [1] represents 1
// [1,1] represents 3
// [0,0,0,1] represents 8
//
(* ****** ****** *)
//
extern
fun // 5 points // successor
succ_intrep(intrep): intrep
//
extern
fun // 5 points // addition
add_intrep_intrep(intrep, intrep): intrep
//
extern
fun // 5 points // multiplication
mul_int_intrep(int, intrep): intrep
//
extern
fun // 5 points // multiplication
mul_intrep_intrep(intrep, intrep): intrep
//
(* ****** ****** *)
//
extern
fun // 5 points
intrep_make_int(int): intrep // for non-negative int
//
(* ****** ****** *)
//
extern
fun // 5 points
print_intrep(intrep): int
//
(* ****** ****** *)

(*
//
Hacking for fun:
Please study the code in the following page and then
change it to create version that can handle large factorials:
https://ats-lang.github.io/DOCUMENT/ATS2FUNCRASH/LECTURE/06-20/CODE/Factorial.html
//
// For instance, you should be able to find the factorial of 1000.
//
*)
extern
fun // 10 *bonus* points
Factorial(n: int): intrep
//
(* ****** ****** *)

(* end of [assign04.dats] *)
