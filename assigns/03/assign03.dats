(* ****** ****** *)
//
// Title:
// Concepts of
// Programming Languages
// Course: CAS CS 320
//
// Semester: Summer I, 2018
//
// Classroom: CAS 220
// Class Time: MTWR 2:00-4:00
//
// Instructor: Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Assign03: 25 points
//
// Out date: Thu, May 31, 2018
// Due date: Tue, Jun 05, 2018 // by midnight
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS\
/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

(*
//
05 points
//
Write a function to return the last element of a nonempty
list. You may assume that the input of this function is a
non-empty list.
*)
extern
fun
mylist0_last(xs: list0(int)): int

(* ****** ****** *)

(*
//
05 points
//
Write a function to return the length of a give list.
It is required that your implementation is tail-recursive.
*)
extern
fun
mylist0_length(xs: list0(int)): int

(* ****** ****** *)
//
(*
//
10 points
//
Please implement the cross product of two given lists:
//
For instance, given xs = (1, 2) and ys = (3, 4, 5), the
following list should be returned:
//
((1, 3), (1, 4), (1, 5), (2, 3), (2, 4), (2, 5))
//
*)
extern
fun
mylist0_cross
  (xs: list0(int), ys: list0(int)): list0($tup(int, int))
//
(* ****** ****** *)

fun
mylist0_mcons
(
  x0: int, xss: list0(list0(int))
) : list0(list0(int)) =
(
  case+ xss of
  | list0_nil() =>
    list0_nil()
  | list0_cons(xs, xss) =>
    list0_cons
      (list0_cons(x0, xs), mylist0_mcons(x0, xss))
    // end of [list0_cons]
)
                  
(* ****** ****** *)
(*
//
10 points
//
Given a list xs and an integer n <= length(xs),
[mylist0_choose] returns a list of lists consisting of
all the sublists of [xs] containing [n] elements
//
For instance, mylist0_choose([1,2,3,4], 2) returns the
following one:
    ((1,2), (1,3), (1,4), (2,3), (2,4), (3,4))
//
*)
extern
fun
mylist0_choose
  (xs: list0(int), n: int): list0(list0(int))
//
(* ****** ****** *)

(* end of [assign03.dats] *)
