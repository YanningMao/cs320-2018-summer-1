(* ****** ****** *)
//
// HX:
// How to compile:
// myatscc assign03_sol.dats
//
// How to test it:
// ./assign03_sol_dats
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

#include "./../assign03.dats"

(* ****** ****** *)

(*
implement
mylist0_length(xs) = ...
*)

(* ****** ****** *)

(*
implement
mylist0_cross(xs, ys) = ...
*)

(* ****** ****** *)

(*

implement
mylist0_choose(xs, n) = ...

*)

(* ****** ****** *)

(* end of [assign03_sol.dats] *)
