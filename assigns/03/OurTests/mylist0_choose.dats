#include "./../MySolution/assign03_sol.dats"
// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests

//
(* #define :: list0_cons *)

val () = println!("\nTesting on an empty list.\n\n
Result should be empty.\n Actual result is ", mylist0_choose(nil0(), 3))


val () = println!("\nTesting on (2, 4, 5, 1, 3) with n=5.\n\n
Result should be same.\n Actual result is ",
mylist0_choose(2 :: 4 :: 5 :: 1 :: 3 :: nil0(),  5))


val () = println!("\nTesting on (2, 4, 5, 1, 3) with n=0.\n\n
Result should be empty.\n Actual result is ",
mylist0_choose(2 :: 4 :: 5 :: 1 :: 3 :: nil0(), 0))


val () = println!("\nTesting singleton list.\n
Result should be singleton 8.\n Actual result is ",
mylist0_choose(list0_sing(8), 1))


val () = println!("\nTesting on (~5, 3, 2147483647, 11111, ~327641) with n=3.\n  Actual result is ",
mylist0_choose(g0ofg1($list{int}(~5, 3, 2147483647, 11111, ~327641)), 3))


val () = println!("\nTesting on (1, 2, 3, 4) with n=2.\n  Actual result is ",
mylist0_choose(g0ofg1($list{int}(1, 2, 3, 4)), 2))
//

val () = println!()
val () = println!("Testing on xs = () and n = 0\n\n Result should be ()\n Result matches? ", list0_is_nil(mylist0_choose(nil0(), 0)))
val () = println!()
val () = println!()
val () = println!("Testing on xs = () and n = 0\n\n Result should be ()\n Result matches? ", list0_is_nil(mylist0_choose(nil0(), 2147483647)))
val () = println!()
val () = println!()
val () = println!("Testing on xs = (-330) and n = -1\n\n Result should be ()\n Result matches? ", list0_is_nil(mylist0_choose(g0ofg1($list{int}(~330)), ~1)))
val () = println!()
val () = println!()
val () = println!("Testing on xs = (-330) and n = 0\n\n Result should be \n Actual result is ", mylist0_choose(g0ofg1($list{int}(~330)), 0))
val () = println!()
val () = println!()
val () = println!("Testing on xs = (-330) and n = 1\n\n Result should be -330\n Actual result is ", mylist0_choose(g0ofg1($list{int}(~330)), 1))
val () = println!()
val () = println!()
val () = println!("Testing on xs = (-330) and n = 2\n\n Result should be ()\n Result matches? ", list0_is_nil(mylist0_choose(g0ofg1($list{int}(~330)), 2)))
val () = println!()
val () = println!()
val () = println!("Testing on xs = (-314159, 271828, -330, 237) and n = -1\n\n Result should be ()\n Result matches? ", list0_is_nil(mylist0_choose(g0ofg1($list{int}(~314159, 271828, ~330, 237)), ~1)))
val () = println!()
val () = println!()
val () = println!("Testing on xs = (-314159, 271828, -330, 237) and n = 0\n\n Result should be\n Actual result is ", mylist0_choose(g0ofg1($list{int}(~314159, 271828, ~330, 237)), 0))
val () = println!()
val () = println!()
val () = println!("Testing on xs = (-314159, 271828, -330, 237) and n = 1\n\n Result should be -314159, 271828, -330, 237\n Actual result is ", mylist0_choose(g0ofg1($list{int}(~314159, 271828, ~330, 237)), 1))
val () = println!()
val () = println!()
val () = println!("Testing on xs = (-314159, 271828, -330, 237) and n = 2\n\n Result should be -314159, 271828, -314159, -330, -314159, 237, 271828, -330, 271828, 237, -330, 237\n Actual result is ", mylist0_choose(g0ofg1($list{int}(~314159, 271828, ~330, 237)), 2))
val () = println!()
val () = println!()
val () = println!("Testing on xs = (-314159, 271828, -330, 237) and n = 3\n\n Result should be -314159, 271828, -330, -314159, 271828, 237, -314159, -330, 237, 271828, -330, 237\n Actual result is ", mylist0_choose(g0ofg1($list{int}(~314159, 271828, ~330, 237)), 3))
val () = println!()
val () = println!()
val () = println!("Testing on xs = (-314159, 271828, -330, 237) and n = 4\n\n Result should be -314159, 271828, -330, 237\n Actual result is ", mylist0_choose(g0ofg1($list{int}(~314159, 271828, ~330, 237)), 4))
val () = println!()
val () = println!()
val () = println!("Testing on xs = (-314159, 271828, -330, 237) and n = 5\n\n Result should be ()\n Result matches? ", list0_is_nil(mylist0_choose(g0ofg1($list{int}(~314159, 271828, ~330, 237)), 5)))
val () = println!()

