#include "./../MySolution/assign03_sol.dats"
// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests

val () = println!()
val () = println!("Testing on xs = () and ys = (). Result should be (). Result matches? ",  list0_is_nil(mylist0_cross(nil0(), nil0())))
val () = println!()
val () = println!("Testing on xs = (324, 32, 4 ,234) and ys = (). Result should be (). Result matches? ",  list0_is_nil(mylist0_cross(g0ofg1($list{int}(324, 32, 4, 234)), nil0())))
val () = println!()
val () = println!("Testing on xs = () and ys = (324, 32, 4 ,234). Result should be (). Result matches? ",  list0_is_nil(mylist0_cross(nil0(), g0ofg1($list{int}(324, 32, 4, 234)))))
val () = println!()
val () = println!("Testing on xs = (657, 41514, 5236) and ys = (-2147283678).\n\n Result should be $tup(657, -2147283678), $tup(41514, -2147283678), $tup(5236, -2147283678)\n Actual result is ", mylist0_cross(g0ofg1($list{int}(657, 41514, 5236)), list0_make_sing(~2147283678)))
val () = println!()
val () = println!()
val () = println!("Testing on xs = (-2147283678) and ys = (657, 41514, 5236).\n\n Result should be $tup(-2147283678, 657), $tup(-2147283678, 41514), $tup(-2147283678, 5236)\n Actual result is ", mylist0_cross(list0_make_sing(~2147283678), g0ofg1($list{int}(657, 41514, 5236))))
val () = println!()
val () = println!()
val () = println!("Testing on xs = (-314156, 320) and ys = (2718, 6021023, -220).\n\n Result should be $tup(-314156, 2718), $tup(-314156, 6021023), $tup(-314156, -220), $tup(320, 2718), $tup(320, 6021023), $tup(320, -220)\n Actual result is ", mylist0_cross(g0ofg1($list{int}(~314156, 320)), g0ofg1($list{int}(2718, 6021023, ~220))))
val () = println!()
