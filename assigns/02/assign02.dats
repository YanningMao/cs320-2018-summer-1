(* ****** ****** *)
//
// Title:
// Concepts of
// Programming Languages
// Course: CAS CS 320
//
// Semester: Summer I, 2018
//
// Classroom: CAS 220
// Class Time: MTWR 2:00-4:00
//
// Instructor: Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Assign02: 30 points
//
// Out date: Tue, May 29, 2018
// Due date: Fri, Jun 01, 2018 // by midnight
//
(* ****** ****** *)
//
#define
ATS_PACKNAME "ASSIGN02"
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

// In this assigmnemt you will be solving some of the first problems in CS 111 in ATS

(* ****** ****** *)
//
// HX: 5 points
// HX: you may assume that the given list is non-empty
// Complete Task 2
// http://www.cs.bu.edu/courses/cs111/old/16fall/labs/lab3.html#task-2-designing-a-recursive-function
//
extern fun min_val(xs: list0(int)): int

(* ****** ****** *)
//
// HX: 5 points
// Complete Task 3
// http://www.cs.bu.edu/courses/cs111/old/16fall/labs/lab4.html#task-3-designing-a-recursive-function
//
extern fun remove_spaces(xs: list0(char)): list0(char)

(* ****** ****** *)
//
// HX: 10 points
// Complete problem 2
// http://www.cs.bu.edu/courses/cs111/old/16fall/problem_sets/ps3.html#problem-2-caesar-cipher-and-decipher
//
extern fun encipher(xs: list0(char), shift: int): list0(char)
extern fun decipher(xs: list0(char), shift: int): list0(char)
//
(* ****** ****** *)
//
// HX: 10 points
// Complete Task 4 (this is NOT optional)
// http://www.cs.bu.edu/courses/cs111/old/16fall/labs/lab4.html#task-4-optional-design-challenge
//
extern
fun
{a:t@ype}
myslice(xs: list0(a), start:int, finish:int): list0(a)

(* ****** ****** *)

(* end of [assign02.dats] *)
