#include "./../MySolution/assign02_sol.dats"
// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests

val xs = g0ofg1($list{int}(23, 2, 3, 4))
val() = println!("The min val in the list is 2:", min_val(xs))


val xs = g0ofg1($list{int}(23, 0, 3, 4))
val() = println!("The min val in the list is 0:", min_val(xs))

val xs = g0ofg1($list{int}(10000000))
val() = println!("The min val in the list is 10000000:", min_val(xs))





