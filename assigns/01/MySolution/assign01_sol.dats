(* ****** ****** *)
//
// HX:
// How to compile:
// myatscc assign01_sol.dats
//
// How to test it:
// ./assign01_sol_dats
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

#include "./../assign01.dats"

(* ****** ****** *)

(*
implement
try_fact() = ...
*)

(* ****** ****** *)

(*
implement sumup_3_5(n0) = ...
*)

(* ****** ****** *)

(*
implement intsqrt(n0) = ...
*)

(* ****** ****** *)

(* end of [assign01_sol.dats] *)
