(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

(*
fun sq(x: int): int = x * x
*)
val sq = lam(x: int): int => x * x

(* ****** ****** *)

val () = println! ("sq(2) = ", sq(2))
val () = println! ("sq(3) = ", sq(3))
val () = println! ("sq(4) = ", sq(4))
val () = println! ("sq(5) = ", sq(5))

(* ****** ****** *)

fun
twice(f: int -> int, x: int): int = f(f(x))

(* ****** ****** *)

val sq2 = lam(x: int): int => twice(sq, x)

(* ****** ****** *)

val () = println! ("sq2(2) = ", sq2(2))
val () = println! ("sq2(3) = ", sq2(3))

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

(* end of [lambda.dats] *)
