(* ****** ****** *)
(*
fact(n) = 1 * 2 * ... * n
*)
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

fun fact(n: int): int = let
  fun
  loop(i: int, res: int): int =
  if i < n then loop(i+1, (i+1)*res) else res
in
  loop(0, 1)
end

(* ****** ****** *)

local

fun
loop(n: int, res: int): int =
if n > 0 then loop(n-1, n*res) else res

in

fun fact(n: int): int = loop(n, 1)

end // end of [local]

(* ****** ****** *)

(*
implement
main0() =
println!("fact(10) = ", fact(10))
*)
implement
main0(argc, argv) = let
//
val inp =
(
if argc > 1
  then g0string2int(argv[1]) else 10
) : int
//
in
  println! ("fact(", inp, ") = ", fact(inp))
end // end of [main0]

(* ****** ****** *)

(* end of [fact2.dats] *)
