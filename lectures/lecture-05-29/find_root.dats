#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

fun
find_root
(
f: int -<cloref1> int
): int = loop(0) where
{
  fun loop(i: int): int = if f(i) = 0 then i else loop(i+1)
}

implement
main0() =
println!
("find_root((x-11)*(x+10)) = "
, find_root(lam(x) => (x-11)*(x+10)))

(* ****** ****** *)

(* end of [find_root.dats] *)
