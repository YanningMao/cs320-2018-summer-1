#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

fun
for_loop
(
start: int
,
finish: int
,
fwork: int -<cloref1> void
): void = loop(start) where
{
  fun loop(i: int): void =
    if i < finish then (fwork(i); loop(i+1)) // else ()
}

implement
main0() = () where
{
  val () =
  for_loop(1, 10, lam(i) => println!(i, "*", i, " = ", i * i))
}

(* ****** ****** *)

(* end of [for_loop.dats] *)
