(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

extern
fun
{a,b:t@ype}
mylist0_zip
( xs: list0(a)
, ys: list0(b)): list0($tup(a, b))

extern
fun
{a
,b:t@ype}
{c:t@ype}
mylist0_map2
( xs: list0(a)
, ys: list0(b)
, fopr: (a, b) -<cloref1> c): list0(c)

(* ****** ****** *)

implement
{a,b}
mylist0_zip
  (xs, ys) =
  mylist0_map2<a,b><$tup(a, b)>
  (xs, ys, lam(x, y) => $tup(x, y))
(*
implement
{a,b}
mylist0_zip
  (xs, ys) =
  zip(xs, ys) where
{
//
fun zip
( xs: list0(a)
, ys: list0(b)): list0($tup(a, b)) =
case (xs, ys) of
| ( nil0(), _) => nil0()
| ( _, nil0()) => nil0()
| ( cons0(x, xs)
  , cons0(y, ys) ) =>
    cons0($tup(x, y), zip(xs, ys))
//
} // end of [mylist0_zip]
*)

(* ****** ****** *)

implement
{a,b}{c}
mylist0_map2
  (xs, ys, fopr) =
  map2(xs, ys) where
{
//
fun map2
( xs: list0(a)
, ys: list0(b)): list0(c) =
case (xs, ys) of
| ( nil0(), _) => nil0()
| ( _, nil0()) => nil0()
| ( cons0(x, xs)
  , cons0(y, ys) ) =>
    cons0(fopr(x, y), map2(xs, ys))
//
} // end of [mylist0_map2]

(* ****** ****** *)

(* end of [zip.dats] *)
