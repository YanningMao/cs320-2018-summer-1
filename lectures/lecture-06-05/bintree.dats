(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)
//
datatype
bintree(a:t@ype) =
| bintree_nil of ()
| bintree_cons of (bintree(a), a, bintree(a))
//
(* ****** ****** *)

extern
fun
{b:t@ype}
{a:t@ype}
bintree_fold
(bt: bintree(a), ini: b, fopr: (b, a, b) -<cloref1> b): b

implement
{b}{a}
bintree_fold
  (bt, ini, fopr) =
(
case+ bt of
| bintree_nil() => ini
| bintree_cons(tl, x, tr) =>
  fopr
  (
  bintree_fold(tl, ini, fopr)
  ,
  x
  ,
  bintree_fold(tr, ini, fopr)
  )
)

(* ****** ****** *)

extern
fun
{a:t@ype}
bintree_height : bintree(a) -> int

implement
{a}
bintree_height(bt) =
bintree_fold<int><a>
(bt, 0, lam(hl, _, hr) => 1+max(hl, hr))

extern
fun
{a:t@ype}
bintree_minpath : bintree(a) -> int

implement
{a}
bintree_minpath(bt) =
bintree_fold<int><a>
(bt, 0, lam(hl, _, hr) => 1+min(hl, hr))

(* ****** ****** *)

extern
fun
{a:t@ype}
bintree_size : bintree(a) -> int

implement
{a}
bintree_size(bt) =
bintree_fold<int><a>
(bt, 0, lam(sl, _, sr) => 1 + (sl + sr))

(* ****** ****** *)

extern
fun
{a:t@ype}
bintree_is_perfect : bintree(a) -> bool

(*
implement
{a}
bintree_is_perfect(bt) =
(
case+ bt of
| bintree_nil() => true
| bintree_cons(tl, _, tr) =>
  (
    bintree_is_perfect(tl)
    &&  
    bintree_is_perfect(tr)
    &&
    (bintree_height(tl) = bintree_height(tr))
  )
)
*)

implement
{a}
bintree_is_perfect(bt) = let

fun
htopt
(
bt: bintree(a)
) : option0(int) =
(
case+ bt of
| bintree_nil() => Some0(0)
| bintree_cons(tl, _, tr) => let
    val hl = htopt(tl)
    and hr = htopt(tr)
  in
    case+ (hl, hr) of
    | (None0(), _) => None0()
    | (_, None0()) => None0()
    | (Some0(hl), Some0(hr)) =>
      if hl = hr then Some0(1+max(hl,hr)) else None0()
  end
)

in
  (
  case+ ht of
  | None0() => false
  | Some0(ht) => true
  ) where
  {
    val ht = htopt(bt)
  }
end // end of [bintree_is_perfect]

(* ****** ****** *)

implement
{a}
bintree_is_perfect(bt) = let

exception NotPerfectExn of ()

fun
htexn
(
bt: bintree(a)
) : int =
(
case+ bt of
| bintree_nil() => 0
| bintree_cons(tl, _, tr) => let
    val hl = htexn(tl)
    and hr = htexn(tr)
  in
    if hl = hr
      then 1+max(hl,hr) else $raise NotPerfectExn()
    // end of [if]
  end
)

in

  try
    ignoret(htexn(bt)); true
  with
    | ~NotPerfectExn() => false
  // end of [try]

end // end of [bintree_is_perfect]

(* ****** ****** *)

#define nil bintree_nil
#define cons bintree_cons

(* ****** ****** *)

val bt1 = cons(nil(), 1, nil())
val bt2 = cons( bt1 , 2,  bt1 )
val bt3 = cons( bt2 , 2,  bt2 )
val bt4 = cons( bt2 , 2,  bt3 )

(* ****** ****** *)
//
val ((*void*)) =
println!
("bintree_is_perfect(bt3) = ", bintree_is_perfect(bt3))
val ((*void*)) =
println!
("bintree_is_perfect(bt4) = ", bintree_is_perfect(bt4))
//
(* ****** ****** *)

(* end of [bintree.dats] *)
