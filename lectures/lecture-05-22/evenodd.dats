(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

extern
fun isevn : int -> bool
extern
fun isodd : int -> bool

implement
isevn(n) =
if n > 0 then isodd(n-1) else true
implement
isodd(n) =
if n > 0 then isevn(n-1) else false

(* ****** ****** *)

implement
main0() =
(
println!("isevn(10) = ", isevn(10));
println!("isodd(10) = ", isodd(10));
)

(* ****** ****** *)

(* end of [evenodd.dats] *)
